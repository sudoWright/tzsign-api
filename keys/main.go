package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/x509"
	"encoding/hex"
	// "encoding/pem"
	"fmt"

	"github.com/gorilla/securecookie"
)

// func encode(privateKey *ecdsa.PrivateKey, publicKey *ecdsa.PublicKey) (string, string) {
//     x509Encoded, _ := x509.MarshalECPrivateKey(privateKey)
	
// 	str := hex.EncodeToString(x509Encoded)

// 	fmt.Println(str)

// 		pemEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})

//     x509EncodedPub, _ := x509.MarshalPKIXPublicKey(publicKey)
//     pemEncodedPub := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

//     return string(pemEncoded), string(pemEncodedPub)
// }

func generateAuthKey() (string) {
	privateKey, _ := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	// publicKey := &privateKey.PublicKey

	x509PrivKey, _ := x509.MarshalECPrivateKey(privateKey)
	// x509PubKey, _ := x509.MarshalPKIXPublicKey(publicKey)
	// privKeyEncoded := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})
	// pubKeyEncoded := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})

	return hex.EncodeToString(x509PrivKey)
}

func generateSessionKey() (string) {
	key := securecookie.GenerateRandomKey(32);

	return hex.EncodeToString(key)
}

func main() {
	// privateKey, _ := ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	// publicKey := &privateKey.PublicKey

	// fmt.Println(privateKey)

    // encPriv, encPub := encode(privateKey, publicKey)

    // fmt.Println(encPriv)
    // fmt.Println(encPub)

	AuthKey := generateAuthKey();
	SessionHashKey := generateSessionKey();
	SessionBlockKey := generateSessionKey();

	fmt.Println("AuthKey=" + AuthKey)
	fmt.Println("SessionHashKey=" + SessionHashKey)
	fmt.Println("SessionBlockKey=" + SessionBlockKey)
}
// 
// 3081a40201010430d406bce5f86a1c21915c05dd300ecfefbbdcc42388d081cac6ffb0bca0c23c7c478594872e5b0bbbe8959aa3a6e57cfba00706052b81040022a164036200044c4d707713693ac695a165ee169e55abba2a72f48b72856a8c752f53803ddeeea82eae6d5ef9e2044eea093ed49c7efb4fa5a0338461cc702d1a4c7e04ee1eed21155cee0c0e14de8b33b0d414ebd097e86238ab809af4f24eb3c3d2f23fee77
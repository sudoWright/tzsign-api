package contract

import (
	"math/big"

	"github.com/btcsuite/btcd/btcec"
	"github.com/tendermint/tendermint/crypto/secp256k1"
)

// Following functions are a fix for Secp256P1 Curve
// Original function of tendermint sdk hash the message
// See https://github.com/tendermint/tendermint/blob/v0.35.9/crypto/secp256k1/secp256k1.go#L198

// Read Signature struct from R || S. Caller needs to ensure
// that len(sigStr) == 64.
func signatureFromBytes(sigStr []byte) *btcec.Signature {
	return &btcec.Signature{
		R: new(big.Int).SetBytes(sigStr[:32]),
		S: new(big.Int).SetBytes(sigStr[32:64]),
	}
}

// VerifySignature verifies a signature of the form R || S.
// It rejects signatures which are not in lower-S form.
func VerifySignatureSecp256k1 (pubKey secp256k1.PubKey, payloadHash []byte, sigStr []byte) bool {
	if len(sigStr) != 64 {
		return false
	}

	pub, err := btcec.ParsePubKey(pubKey, btcec.S256())
	if err != nil {
		return false
	}

	// parse the signature:
	signature := signatureFromBytes(sigStr)
	// Reject malleable signatures. libsecp256k1 does this check but btcec doesn't.
	// see: https://github.com/ethereum/go-ethereum/blob/f9401ae011ddf7f8d2d95020b7446c17f8d98dc1/crypto/signature_nocgo.go#L90-L93
	var secp256k1halfN = new(big.Int).Rsh(btcec.S256().N, 1)
	if signature.S.Cmp(secp256k1halfN) > 0 {
		return false
	}

	return signature.Verify(payloadHash, pub)
}
